// load environment variables
require('dotenv').config();

// grab our dependencies
const express = require('express');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const app = express();
const port = process.env.PORT || 8080;
const expressLayouts = require('express-ejs-layouts');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const flash = require('connect-flash');
const expressValidator = require('express-validator');
const moment = require('moment');


moment.locale('pt-BR');

mongoose.Promise = global.Promise;

// config our application
// set sessions and cookie parser
app.use(cookieParser());
app.use(session({
    secret: process.env.SECRET,
    cookie: {
        maxAge: 600000
    },
    resave: false,
    saveUninitialized: false
}));

app.use(flash());

// initiate passport session
app.use(passport.initialize());
app.use(passport.session());

// tell express where to look for our static files
app.use(express.static(__dirname + "/public"));

// set ejs as our templating engine
app.set("view engine", "ejs");
app.use(expressLayouts);

// connect to our database
mongoose.connect(process.env.DB_URI);

// use body parser to grab info from a form
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(expressValidator());

app.use(function(req, res, next){
	res.locals.user = req.user || null;
	next();
})

// set our routes
app.use(require("./app/routes"));

// start our server
app.listen(port, function() {
    console.log(`App listening on http://localhost:${port}`);
});
