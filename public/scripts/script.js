var nice = false;

$(document).ready(function(){
	 if (Modernizr.touch) {
            // show the close overlay button
            $(".close-overlay").removeClass("hidden");
            // handle the adding of hover class when clicked
            $(".img").click(function(e){
                if (!$(this).hasClass("hover")) {
                    $(this).addClass("hover");
                }
            });
            // handle the closing of the overlay
            $(".close-overlay").click(function(e){
                e.preventDefault();
                e.stopPropagation();
                if ($(this).closest(".img").hasClass("hover")) {
                    $(this).closest(".img").removeClass("hover");
                }
            });
        } else {
            // handle the mouseenter functionality
            $(".img").mouseenter(function(){
                $(this).addClass("hover");
            })
            // handle the mouseleave functionality
            .mouseleave(function(){
                $(this).removeClass("hover");
            });
        }

        $('#imgPortfolio').on('show.bs.modal', function (e) {
        var image = $(e.relatedTarget).data('image');
        $(".showimage").attr("src", image);
    });

});


$("#postSingle").html($("#postSingle").text());
$("#portfolioText").html($("#portfolioText").text());

$(".posts-html").each(function(){
		$(this).html($(this).text());
});

 $('.twitterShare').click(function(event) {
		 var width  = 575,
		        height = 400,
		        left   = ($(window).width()  - width)  / 2,
		        top    = ($(window).height() - height) / 2,
		        url    = this.href,
		        opts   = 'status=1' +
		                 ',width='  + width  +
		                 ',height=' + height +
		                 ',top='    + top    +
		                 ',left='   + left;
		    window.open(url, 'twitter', opts);
		 
		    return false;
 });

$('.rotate').on('click', function(){
		$(this).toggleClass('active');
});

$(".delete").on('click', function(){
	$(this).parent().hide();
});

$('#myModal').on('hidden.bs.modal', function () {
  	location.reload();
});

$(document).on('click', '[data-toggle="lightbox"]', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
});
