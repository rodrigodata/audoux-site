// require dependencies
const express = require("express");
const router = express.Router();
var multer = require('multer');
var upload = multer({ dest: 'public/uploads/' });
const mainController = require("./controllers/main.controller");
const postsController = require("./controllers/posts.controller");
const portfolioPostsController = require("./controllers/portfolio.posts.controller");
const usersController = require("./controllers/users.controller");
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

// export Router
module.exports = router;

// define our routes
// home route
router.get("/", portfolioPostsController.showPosts);

// blog route
router.get("/blog", postsController.showPosts , function(req, res){});

// create a new post
router.get("/blog/create", ensureAuth, postsController.showCreate);
router.post("/blog/create", ensureAuth, postsController.processCreate);

// show a single blog post by slug
router.get("/blog/:slug", postsController.showSinglePost);

// edit a post
router.get("/blog/:slug/edit", ensureAuth, postsController.showEdit);
router.post("/blog/:slug", ensureAuth, postsController.processEdit);

// delete post
router.get("/blog/:slug/delete", ensureAuth, postsController.deletePost);

// create a new portfolio post
router.get("/portfolio/create", ensureAuth, portfolioPostsController.showCreate);
router.post("/portfolio/create", ensureAuth, upload.any(), portfolioPostsController.showProcessCreate);

// edit a portfolio
router.get("/portfolio/:slug/edit", ensureAuth, portfolioPostsController.showEdit);
router.post("/portfolio/:slug", ensureAuth, upload.any(), portfolioPostsController.showProcessEdit);

// show a single blog post by slug
router.get("/portfolio/:slug", portfolioPostsController.showSinglePost);

// delete portfolio post
router.get("/portfolio/:slug/delete", ensureAuth, portfolioPostsController.deletePost);

// show contact pages
router.get("/contato", postsController.showContato);
router.post("/contato", postsController.enviarContato);

// show about me page
router.get("/sobremim", postsController.showSobre);

// show admin page
router.get("/admin", ensureAuth, usersController.showAdmin);
router.get("/admin/portfolio", ensureAuth, usersController.showAdminPortfolio);
router.get("/admin/blog", ensureAuth, usersController.showAdminBlog);

// show log in page
router.get("/admin/login", usersController.showLogin);
router.post("/admin/login", passport.authenticate('local', {sucessRedirect: '/', failureRedirect: '/admin/login', failureFlash: true}),  usersController.userLogin);

// create a user
//router.get("/admin/register", usersController.showCreate);
//router.post("/admin/register", usersController.processCreate);

// show users
router.get("/admin/users", ensureAuth, usersController.showUsers);

// logout user
router.get("/admin/logout", ensureAuth, usersController.processLogout);

function ensureAuth(req, res, next){
	if(req.isAuthenticated()){
		return next()
	} else {
		req.flash('error', 'Você não está logado!');
		res.redirect('/admin/login');
	}
}