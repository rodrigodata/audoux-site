module.exports = {
    // show home page
    showBlog: function(req, res) {
        res.render('pages/blog');
    },
    showContato: function(req, res) {
        res.render('pages/contato');
    }
};
