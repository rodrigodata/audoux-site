const Post = require('../models/post');
const moment = require('moment');
var api_key = 'key-bb3eab5052f06954b0e1be675bd47b3d';
var domain = 'sandbox47dba68eed594e8aa84591a0b1a472b9.mailgun.org';
var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});

module.exports = {
    showPosts: showPosts,
    showSinglePost: showSinglePost,
    showCreate: showCreate,
    processCreate: processCreate,
    showEdit: showEdit,
    processEdit: processEdit,
    deletePost: deletePost,
    showContato: showContato,
    enviarContato: enviarContato,
    showSobre: showSobre
}

function enviarContato(req, res) {

    // validate information
    req.checkBody('email', 'O Campo E-mail é Obrigatório!').notEmpty();
    req.checkBody('name', 'O Campo Nome é Obrigatório!').notEmpty();
    req.checkBody('message', 'O Campo Mensagem é Obrigatório!').notEmpty();

    // if there are errors
    const errors = req.validationErrors();
    if (errors) {
        req.flash('errors', errors.map(err => err.msg));
        return res.redirect('/contato');
    }

    var data = {
        from: ' Novo Contato em Audoux.me <postmaster@sandbox47dba68eed594e8aa84591a0b1a472b9.mailgun.org>',
        to: 'joaudoux@gmail.com',
        subject: req.body.subject,
        html: "<html><h2>Novo Contato</h2><br>" +
             "<b>De:</b> " + req.body.name + "<br>" +
             "<b>Email:</b> " + req.body.email + "<br>" +
             "<h3>Mensagem:</h3>" + "<br>" +
              req.body.message +
             "</html>"
    };

    if(data){
         mailgun.messages().send(data, function (error, body) {
         if(error)
            throw error;
         req.flash('success', data);
         });
    }
}

function showContato(req, res) {
            res.render('pages/contato', {
                errors: req.flash('errors'),
                success: req.flash('success')
            });
}

function showSobre(req, res){
            // gera rota para /sobremim
            res.render('pages/sobremim');
}

function showPosts(req, res) {
        // limite de posts a serem exibidos em /blog por pagina
        var limitePage = 5;

        Post.paginate ({}, {page: req.query.page || 1, limit: limitePage, sort: {createdAt: "-1"}}, function(err, posts) {
        // if we cant reach posts
        if (err) {
            res.status(404);
            res.send("Posts not found!");
        }

        // if we can reach database we'll return the data
        res.render('pages/blog', {
            posts: posts.docs,
            totalPosts: posts.total,
            totalPaginas: posts.pages,
            paginaAtual: posts.page,
            success: req.flash('success')
        });

      });
}

function showSinglePost(req, res) {
    Post.findOne({
        slug: req.params.slug
    }, function(err, post) {
        if (err) {
            res.status(404);
            res.send("Post not found!");
        }   

            res.render('pages/single', {
                post: post,
                success: req.flash('success')
        });
    })
}

// show the create form
function showCreate(req, res) {
    res.render("pages/create", {
        errors: req.flash('errors')
    });
}

// process create function
function processCreate(req, res) {
    // validate information
    req.checkBody('title', 'O Titulo é obrigatório.').notEmpty();
    req.checkBody('text', 'O Conteúdo é obrigatório.').notEmpty();

    // if there are errors
    const errors = req.validationErrors();
    if (errors) {
        req.flash('errors', errors.map(err => err.msg));
        return res.redirect('/blog/create');
    }

    var dayOfWeekDay = moment().format('dddd');
    var dayOfMonth = moment().format('MMM D');
    var dateToSave = dayOfWeekDay + ', ' + dayOfMonth;

     var a = req.body.tags;
     req.body.tags = [];
     var c = a.split(',');
     req.body.tags = c;

    // create a new Post
    const post = new Post({
        title: req.body.title,
        tags: req.body.tags,
        createdAt: moment().format("lll"),
        timeDate: dateToSave,
        text: req.body.text
    });

    post.save(function(err) {
        if (err)
            throw err;
        // set success message
        req.flash('success', 'Post criado com Sucesso!');
        // redirect to the new post
        res.redirect(`/blog/${post.slug}`);
    })
}

// show edit form
function showEdit(req, res) {
    Post.findOne({
        slug: req.params.slug
    }, function(err, post) {
        res.render('pages/edit', {
            post: post,
            errors: req.flash('errors')
        });
    });
}

// process edit
function processEdit(req, res) {
    // validate information
    req.checkBody('title', 'O Titulo é obrigatório.').notEmpty();
    req.checkBody('text', 'O Conteúdo é obrigatório.').notEmpty();

    // if there are errors
    const errors = req.validationErrors();
    if (errors) {
        req.flash('errors', errors.map(err => err.msg));
        return res.redirect(`/blog/${req.params.slug}/edit`);
    }

    // finding current post
    Post.findOne({
        slug: req.params.slug
    }, function(err, post) {
        //updating that post
        post.title = req.body.title;
        post.text = req.body.text;
        post.tags = req.body.tags;
        post.modifiedAt = "Editado em " + moment().format("lll");

        post.save(function(err) {
            if (err)
                throw err;
            req.flash('success', 'Post editado com sucesso!');
            res.redirect(`/blog/${req.params.slug}`);
        });
    })
}

// delete post
function deletePost(req, res) {
    Post.remove({
        slug: req.params.slug
    }, function(err) {
        // set flash data
        req.flash('success', 'Post deletado com sucesso!');
        // redirect to admin painel
        res.redirect('/admin/blog');
    })
}
