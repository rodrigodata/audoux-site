const Users = require('../models/users');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const Post = require('../models/post');
const PostPortfolio = require('../models/post-portfolio');

module.exports = {
	    //showCreate: showCreate,
	    showLogin: showLogin,
	    userLogin: userLogin,
	    //processCreate: processCreate,
	    showAdmin: showAdmin,
	    showAdminPortfolio: showAdminPortfolio,
	    showAdminBlog: showAdminBlog,
	    showUsers: showUsers,
	    processLogout: processLogout
}
/*
function showCreate(req, res){
	res.render("pages/register", {
        errors: req.flash('errors')
    });
}*/
/*
function processCreate(req, res){

	// Validation
	req.checkBody('name', 'Nome é Obrigatório.').notEmpty();
	req.checkBody('email', 'Email é Obrigatório.').notEmpty();
	req.checkBody('email', 'Email não está em formato válido.').isEmail();
	req.checkBody('username', 'Nome de Usuário é Obrigatório.').notEmpty();
	req.checkBody('password', 'Senha é Obrigatória.').notEmpty();
	req.checkBody('password2', 'Senhas não estão iguais.').equals(req.body.password);

	const errors = req.validationErrors();

	if(errors){
			req.flash('errors', errors.map(err => err.msg));
			return res.redirect('/admin/register');
	} 

	const user = new Users({
		username: req.body.username,
		password: req.body.password,
		email: req.body.email,
		name: req.body.name
	})

	Users.createUser(user, function(err, newUser){
		if(err)
			throw err;
		console.log(newUser);
	})

    // set success message
    req.flash('success', 'Usuário criado com Sucesso!');
    // redirect to home
    res.redirect("/");
}*/

function showAdmin(req, res){
	res.render("pages/admin/admin");
}

function showAdminPortfolio(req, res){
	// limit per page
    var limitPage = 10;
        
    PostPortfolio.paginate({}, {page: req.query.page || 1, limit: limitPage, sort: {createdAt: "desc"}}, function(err, posts) {
        // if we cant reach posts
        if (err) {
            res.status(404);
            res.send("Posts not found!");
        }

         // if we can reach database we'll return the data
            res.render('pages/admin/portfolio', {
                posts: posts.docs,
                totalPaginas: posts.pages,
                paginaAtual: posts.page,
                success: req.flash('success')
            });
    });
}

function showAdminBlog(req, res){
	    // limite de posts a serem exibidos em /admin/blog por pagina
        var limitePage = 10;

        Post.paginate ({}, {page: req.query.page || 1, limit: limitePage, sort: {createdAt: "-1"}}, function(err, posts) {
        // if we cant reach posts
        if (err) {
            res.status(404);
            res.send("Posts not found!");
        }

        // if we can reach database we'll return the data
        res.render('pages/admin/blog', {
            posts: posts.docs,
            totalPosts: posts.total,
            totalPaginas: posts.pages,
            paginaAtual: posts.page,
            success: req.flash('success')
        });

      });
}

function showUsers(req, res){
	res.render("pages/admin/users");
}

function showLogin(req, res){
	res.render("pages/login", {
		success: req.flash('success'),
		error: req.flash('error')
	});
}

passport.use(new LocalStrategy(
  function(username, password, done) {
  	Users.getUserByUserName(username, function(err, user){
  		if(err)
  			throw err;
  		if(!user){
  			return done(null, false, { message: "Usuário não encontrado!"});
  		}

  		Users.comparePassword(password, user.password, function(err, isMatch){
  			if(err)
  				throw err;
  			if(isMatch){
  				return done(null, user);
  			} else {
  				return done(null, false, { message: "Senha Inválida!"});
  			}
  		})
  	})
  }
));

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  Users.findById(id, function(err, user) {
    done(err, user);
  });
});

function userLogin(req, res){
	req.flash('success', 'Login Efetuado com Sucesso!');
	console.log(res.locals);
	res.redirect('/');
}	

function processLogout(req, res, next){
	req.logout();
	req.flash('success', 'Logout efetuado com sucesso');
	res.redirect('/admin/login');
}

