require('dotenv').config('/.env');

const Post = require("../models/post-portfolio");
const moment = require('moment');
const fs = require('fs');
var multer  = require('multer');
var upload = multer({ dest: 'public/uploads/' });
var cloudinary = require('cloudinary');

cloudinary.config({ 
          cloud_name: "audoux-me",
          api_key: "871692912249115",
          api_secret: "SK30fXI3CYfq3N1hH6MSLXV8epI"
});

module.exports = {
    showCreate: showCreate,
    showProcessCreate: showProcessCreate,
    showSinglePost: showSinglePost,
    showPosts: showPosts,
    showEdit: showEdit,
    showProcessEdit: showProcessEdit,
    deletePost: deletePost
}

function showSinglePost(req, res) {
    Post.findOne({
        slug: req.params.slug
    }, function(err, post) {
        if (err) {
            res.status(404);
            res.send("Post not found!");
        }

        res.render('pages/portfolioSingle', {
            post: post,
            success: req.flash('success')
        });
    })
}

function showPosts(req, res) {

    // limit per page
    var limitPage = 9;
        
    Post.paginate({}, {page: req.query.page || 1, limit: limitPage, sort: {createdAt: "desc"}}, function(err, posts) {
        // if we cant reach posts
        if (err) {
            res.status(404);
            res.send("Posts not found!");
        }

         // if we can reach database we'll return the data
            res.render('pages/home', {
                posts: posts.docs,
                totalPaginas: posts.pages,
                paginaAtual: posts.page,
                success: req.flash('success')
            });
    });
}

function showEdit(req, res) {
    Post.findOne({
        slug: req.params.slug
    }, function(err, post) {
        res.render('pages/portfolioEdit', {
            post: post,
            errors: req.flash('errors')
        });
    });
}

function showProcessEdit(req, res){

    // validate information
    req.checkBody('title', 'O Titulo é obrigatório.').notEmpty();
    req.checkBody('description', 'O Conteúdo é obrigatório.').notEmpty();

    // if there are errors
    const errors = req.validationErrors();
    if (errors) {
        req.flash('errors', errors.map(err => err.msg));
        return res.redirect(`/portfolio/${req.params.slug}/edit`);
    }

    var img_id;
    var imgUrl;
    var req_files;
    var req_files_path;
    var req_files_length = req.files.length;

    if(req.files.length > 0){
        req_files = req.files[0].filename;
        req_files_path = req.files[0].path;
    }
   
    // finding current post
    Post.findOne({
        slug: req.params.slug
    }, function(err, post) {

        if(req_files_length > 0){
        img_id = req_files;
        cloudinary.uploader.destroy(post.imgId,
            function(result){
                console.log("Imagem " + post.imgId + " foi excluida!");
        });

        cloudinary.uploader.upload(req_files_path, function(result) { 
            console.log("Imagem Salva no cloudinary!");
         }, { public_id: img_id});

            imgUrl = "http://res.cloudinary.com/audoux-me/image/upload/" + img_id + ".png";
            post.img = imgUrl;
            post.imgId = img_id;
            fs.unlink(req.files[0].path);
        }

        //updating that post
        post.title = req.body.title;
        post.description = req.body.description;
        post.modifiedAt = moment().format("lll");

        post.save(function(err) {
            if (err)
                throw err;
            
            req.flash('success', 'Post editado com sucesso!');
            res.redirect(`/portfolio/${req.params.slug}`);
        });
    })
}

function showCreate(req, res) {
    res.render("pages/portfolioCreate", {
        errors: req.flash('errors')
    });
}

function showProcessCreate( req, res) {
    // validate information
    req.checkBody('title', 'O Titulo é obrigatório.').notEmpty();
    req.checkBody('description', 'O Conteúdo é obrigatório.').notEmpty();

    // if there are errors
    const errors = req.validationErrors();
    if (errors) {
        req.flash('errors', errors.map(err => err.msg));
        return res.redirect('/portfolio/create');
    }

    var imgId = req.files[0].filename;
    var dayOfWeekDay = moment().format('dddd');
    var dayOfMonth = moment().format('MMM D');
    var dateToSave = dayOfWeekDay + ', ' + dayOfMonth;

     // create a new Post
    const post = new Post({
        img: "URL",
        imgId: imgId,
        title: req.body.title,
        createdAt: Date.now(),
        timeDate: dateToSave,
        description: req.body.description
    });
    
    cloudinary.uploader.upload(req.files[0].path, function(result) { 
            console.log("Imagem salva no cloudinary.");
            
     }, { public_id: imgId});

    var imgUrl = "http://res.cloudinary.com/audoux-me/image/upload/" + imgId + ".png";
    post.img = imgUrl;

    post.save(function(err) {
        // after saving the image we can then deleted from temp files
        fs.unlink(req.files[0].path);
        // set success message
        req.flash('success', 'Post criado com Sucesso! :D');
        
        // redirect to the new post
        res.redirect(`/portfolio/${post.slug}`);
    })
}

function deletePost(req, res) {

        var imgToDelete;

        Post.findOne({
            slug: req.params.slug
        }, function(err, post) {
        imgToDelete = post.imgId;
        
        Post.remove({
                slug: req.params.slug
        }, function(err) {
       
        // after delete post from mongodb, delete image from cloudinary
        cloudinary.uploader.destroy(imgToDelete,
        function(result){
            console.log("Imagem " + imgToDelete + " foi excluida!");
        });

        // set flash data
        req.flash('success', 'Post deletado com sucesso!');
        // redirect to admin painel
        res.redirect('/admin/portfolio');
        })
     })
}