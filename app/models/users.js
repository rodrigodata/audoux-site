const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');

// create a Schema
const userSchema = new Schema({
	    username: { type: String, unique: true },
	    password: { type: String },
	    role: String,
	    email: String,
	    name: String
});

// create the model
const userModel = module.exports = mongoose.model("users", userSchema);

module.exports.createUser = function(newUser, callback){
	bcrypt.genSalt(10, function(err, salt){
		bcrypt.hash(newUser.password, salt, function(err, hash){
			newUser.password = hash;
			newUser.save(callback);
		})
	})
}

module.exports.getUserByUserName = function(username, callback){
	var query = {username: username};
	userModel.findOne(query, callback);
}

module.exports.comparePassword = function(candidatePassword, hash, callback){
	bcrypt.compare(candidatePassword, hash, function(err, isMatch) {
		if(err)
			throw err;
		callback(null, isMatch);
	})
}
