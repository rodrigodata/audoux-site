const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const fs = require('fs');
const Schema = mongoose.Schema;

// create a schema
const postSchema = new Schema({
    title: String,
    slug: {
        type: String,
        unique: true
    },
    img: String,
    imgId: String,
    createdAt: String,
    timeDate: String,
    modifiedAt: String,
    tags: String,
    description: String
});

// set to default the order of the collections ( in this case, desc order )
mongoosePaginate.paginate.options = {
    sort: { date: -1}
}

postSchema.plugin(mongoosePaginate);

// middleware
// make sure that the slug is created from the name
postSchema.pre("save", function(next) {
    this.slug = slugify(this.title);
    next();
});

// create the model
const postModel = mongoose.model("portfolioPost", postSchema);

// exports
module.exports = postModel;

// function to slugify a name
function slugify(text) {
    return text.toString().toLowerCase()
        .replace(/\s+/g, '-') // Replace spaces with -
        .replace(/[^\w\-]+/g, '') // Remove all non-word chars
        .replace(/\-\-+/g, '-') // Replace multiple - with single -
        .replace(/^-+/, '') // Trim - from start of text
        .replace(/-+$/, ''); // Trim - from end of text
}
